module.exports = {
  presets: [
    '@vue/app',
    ['@babel/preset-env', {
      "targets": {
        // The % refers to the global coverage of users from browserslist
        "browsers": [ ">0.25%"]
      }
    }],

  ]
}
